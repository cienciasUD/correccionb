#include "arbol.h"

void imprimir(nodo *Actual,int n){
	std::cout << "NIVEL: " << n << "  ";
	for(int i = 0; i<Actual->Nclaves; i++){
			cout<<Actual->claves[i]<<",";
	}
	cout<<endl;
	if(!Actual->hoja){
		for(int i=0; i<Actual->Nclaves+1; i++){

			imprimir(Actual->hijos[i] , n+1);
		}
	}

}


int main(){

	arbolB prueba(2);

	//EJEMPLO MAS SIMPLE
	prueba.insertar(12);
	prueba.insertar(11);
	prueba.insertar(10);
	prueba.insertar(9);
	prueba.insertar(8);
	prueba.insertar(7);


	//EJEMPLO MAS COMPLEJO
	/*
	prueba.insertar(12);
	prueba.insertar(11);
	prueba.insertar(10);
	prueba.insertar(9);
	prueba.insertar(8);
	prueba.insertar(7);
	prueba.insertar(6);
	prueba.insertar(5);
	prueba.insertar(4);
	prueba.insertar(3);
	prueba.insertar(2);
	prueba.insertar(1);
	prueba.insertar(0);*/


	cout << "Impresion final" << '\n';
	imprimir(prueba.get_raiz(),0);

	cout << "Fin impresion final" << '\n';

	prueba.borrar(6);
	return 0;
}
