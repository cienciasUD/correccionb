#ifndef ARBOLB_H
#define ARBOLB_H
#include <iostream>

using namespace std;

struct nodo{
	int *claves;
	nodo **hijos;
	int Nclaves;
	bool hoja;
};

class arbolB {
	nodo *raiz;
	int d;
	public:
		arbolB(int ord){
			d = ord;
			raiz = crear_nodo();
			raiz->Nclaves = 0;
			raiz-> hoja = true;
		}

		//DEFINICION PROTOTIPOS
		nodo *crear_nodo();
		nodo *get_raiz ();
		int  dividir_nodo(nodo *actual, nodo *nuevo);
		void insertar(int clave);
		nodo *buscar_nodo(int clave, nodo *Actual, nodo **padre);
		int InsertarNodoNormal(int clave, nodo *Actual);
		void InsertarNodoLleno(int clave, nodo *Actual, nodo *padre);
		nodo *buscar_padre (int clave, nodo *Actual, nodo *padre);
		bool borrar(int clave);
		nodo *Unir_nodos(nodo* hizq, nodo*hDer, nodo*padre);
		nodo *buscar_clave(int clave, nodo* Actual);
};



nodo *arbolB::crear_nodo (){

	nodo* nuevo = new nodo;
	int *clavesA = new int[2*d-1];
	nuevo -> claves= clavesA;
	nuevo->hijos = new nodo* [2*d];
	nuevo->Nclaves=0;
	nuevo->hoja=true;
	return nuevo;
}

nodo *arbolB::get_raiz (){
	return raiz;
}

void arbolB::insertar (int clave){
	nodo *padre= NULL, *Actual=raiz;
	Actual = buscar_nodo( clave, Actual, &padre);
	if (Actual->Nclaves<2*d-1){
		InsertarNodoNormal(clave, Actual);
	}else {
		InsertarNodoLleno(clave, Actual, padre);
	}
}
/*!
\brief "Se busca un nodo en el cual se debe insertar la clave"
\param "clave Clave a agregar"
\param "*Actual apuntador a raiz (inicalmente)"
\param "**padre Apuntador a el apuntador de padre"
\return "El nodo en el cual se debe insertar la clave"
*/
nodo *arbolB::buscar_nodo(int clave, nodo *Actual, nodo **padre){
	int i=0;

	while((i < Actual ->Nclaves) && clave > Actual->claves[i]){ //SE RECORREN LAS CLAVES DE ACTUAL HASTA QUE clave
		//sea menor a la clave(i)
		i++;
	}
	//i va a ser el lugar donde insertar la nueva clave
	if (Actual->hoja){ //Se retorna la raiz
		if (Actual==raiz)
		*padre=NULL;
		return Actual;
	}else{
		*padre= Actual;
		Actual= Actual ->hijos[i]; //Se abaja a el nuevo nodo y se ejecuta buscar_nodo
		return buscar_nodo(clave, Actual, padre);
	}
}

/*!
\brief "Insertar una clave en un nodo que no este lleno aun"
\param "clave La clave a insertar en el nodo"
\param "*Actual Nodo en el cual se va insertar la clave previamente definido por buscar_nodo"
\return "La posicion del nodo en la cual se agrego la clave"
*/
int arbolB::InsertarNodoNormal(int clave, nodo *Actual){
	int i, j;
	i=Actual->Nclaves;
	while((i-1)>=0 && clave< Actual->claves[i-1]){//Se compara la nueva clave con las demas de derecha a izq
		Actual->claves[i]=Actual->claves[i-1];
		if (!Actual->hoja) //si el nodo no es una hoja
		Actual->hijos[i+1]=Actual->hijos[i];
		i--;
	}
	Actual->claves[i]=clave;
	(Actual->Nclaves)++;
	return i;
}

/*!
\brief "Insercion de una clave en un nodo ya lleno, por lo que se tendra que subir la mitad y crear 2 nuevos nodos"
\param "clave La clave a insertar"
\param "*Actual nodo en el cual se insertara la clave"
\param "**padre Padre del nodo a insertar, en el primer llamado sera siempre NULL"
*/
void arbolB::InsertarNodoLleno( int clave, nodo *Actual, nodo *padre){
	int pos, sube;
	nodo *nuevo = crear_nodo();
	//nodo *nuevo = NULL;

	nodo *nivelad = NULL;
	//ANTES HABIA UN DOBLE APUNTADOR EL CUAL NO SE USABA AHORA SE MODIFIC Actual y nuevo en dividir
	sube = dividir_nodo(Actual, nuevo);	//sube es la mitad del arreglo lleno

	if(clave < sube){//Si la clave es menor a el de la mitad ira a la izquierda
		//Actual es la raiz
		//Actual->claves[d-1]=clave; //AQUI ES EL PROBLEMA NO ESTA ORGANIZANDO SOLO LO METE EN CUALQUIER LADO
		InsertarNodoNormal(clave, Actual);
		//(Actual->Nclaves)++;
	}else{//Ira a la derecha de lo contrario
		nuevo->claves[d-1]=clave;
		(nuevo->Nclaves)++;


	}



	if(Actual != raiz && padre->Nclaves< 2*d-1){
		pos= InsertarNodoNormal(sube, padre);
		padre->hijos[pos]=Actual;
		padre->hijos[pos+1]=nuevo;
	}else{
		if (Actual==raiz){
			nivelad=crear_nodo();
			nivelad->hoja = false;
			nivelad->Nclaves=1;
			nivelad->claves[0]=sube;
			nivelad->hijos[0]= Actual;
			nivelad->hijos[1]=nuevo;
			raiz=nivelad;

		}else{
			nivelad=buscar_padre(padre->claves[0], padre, nivelad);
			InsertarNodoLleno(padre->claves[d-1], padre, nivelad);
		}
	}



}

	/*!
	\brief "Se busca el numero de la mitad del arreglo claves"
	\param "*actual"
	\pre "**hermano"
	\return "El valor medio del arreglo claves"
	*/
int arbolB::dividir_nodo(nodo *actual, nodo *nuevo){
	int i;

	//El nuevo va a tener la hoja del actual
	nuevo->hoja=actual->hoja;

	//Se llena nuevo con los elementos de la derecha de actual
	for(i=0; i<d-1; i++){
		nuevo->Nclaves++; //SE AGREGA PUES NUMERO CLAVES ESTABA EN CERO  Y NO HAY NADA QUE AGREGSR
		nuevo->claves[i]=actual->claves[i+d];
	}

	//QUE SE SUPONE QUE HACE ESTE IF?
	if(!actual->hoja)
		for (i=0; i<d; i++)
			nuevo->hijos[i]=actual->hijos[i+d];

	nuevo->Nclaves=actual->Nclaves=d-1;
	cout<<"Mitad: "<<actual->claves[d-1]<<endl;
	return actual->claves[d-1];
	}

nodo *arbolB::buscar_padre(int clave, nodo *Actual, nodo *padre){
	int i=0;
	if(clave == Actual->claves[0])
		return padre;
	else{
		while(i < Actual->Nclaves && clave > Actual->claves[i]) i++;
		padre=Actual;
		Actual= Actual -> hijos[i];
		return buscar_padre(clave, Actual, padre);}
}

bool arbolB::borrar(int clave){
	nodo *nodoBorrar, *Actual = raiz;
	nodoBorrar = buscar_clave(clave, Actual);
	if(nodoBorrar->hoja){
		
	}
	
}

nodo* arbolB::buscar_clave(int clave, nodo* Actual){

	int i;
	if(Actual->claves[0]>clave && !Actual->hoja){
		Actual = Actual->hijos[0];
		return buscar_clave(clave, Actual);
	}else if(Actual->claves[Actual->Nclaves-1]<clave && !Actual->hoja){
		Actual = Actual->hijos[Actual->Nclaves-1];
		return buscar_clave(clave, Actual);
	}
	
	for(i=0;;i++){
		if(Actual->claves[i]<clave && Actual->claves[i+1]>clave){
			Actual = Actual->hijos[i+1];
			return buscar_clave(clave, Actual);
		}else if(Actual->claves[i]==clave){
			return Actual;
		}
	}
	return NULL;
}
#endif
